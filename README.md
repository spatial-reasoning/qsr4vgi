[This project](https://www.uni-bamberg.de/en/sme/research/qsr4vgi/) will investigate Qualitative Spatial Reasoning (QSR) techniques to capture context-sensitive vague spatial information in human-generated vague place descriptions and will develop algorithms that allow geographic databases  to be queried using such descriptions. With our approach developed in this project, we are able to identify places like “the café near the bridge over the river”, which require unnamed entities (café, river, bridge) to be identified from context and vague relations (near) to be interpreted sensibly.

![approach overview](spatial-reasoning.png)

Volunteered Geographic Information (VGI) can be regarded to encompass a broad spectrum of sources for information, ranging from purposefully collected geo-referenced data to implicit geographical references, for example textual descriptions of geographic entities like "a park between the arms of the Regnitz river, near the village Bug". Enabling computer systems to benefit from the latter category of implicit information is particularly relevant since text-based communication is a very natural form of information exchange, in particular considering social media platforms, Tweets, or the Wikipedia encyclopedia. As a consequence, there exist manifold pieces of information that can serve several applications. Methods to access such kind of information automatically may also serve as tools to gain new insights into social aspects of utilization and conception of spatial environments. Before pieces of implicit geographic information contained in text can be exploited, geographic information content has first to be made explicit and interpreted with respect to existing geographical knowledge – this puts a focus on spatial knowledge representation, reasoning, and querying techniques. 

Research questions addressed:

 - To which extent, i.e., which aspects of context-sensitive vague spatial relations, can a qualitative representation capture vague place descriptions?
 - What are the computational properties of integrating distinct statements expressed using such a  representation?
 - How efficient can a geographic database be queried using expressed using the representation?

The aim of this project is to develop (1) a computational model to represent qualitative context-dependent vague spatial relations in symbolic statements, (2) reasoning algorithms capable of integrating several such statements, and (3) query algorithms that allow matching vaguely described places against a geographic database. 
